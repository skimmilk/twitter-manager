Small scripts for scraping Twitter for a corpus, and automating Twitter accounts

Probably schedule it with `cron`

### Requirements
`pip3 install tweepy tqdm markovify`

### Workflow
1. Put your Twitter OAuth creds for scraping Twitter into `streamcreds.py`
    - Advised to use an account you're not posting from, in case they get banned.
2. Use `TweetGenerator.py` to generate some tweets
3. Store the creds of your bots in `creds.txt` 
4. Use Chatter.py to post them user your accounts stored in `creds.txt` 

### File overview
`Chatter.py`
This does your main interaction using the accounts stored in `creds.txt`
`$ python3 Chatter.py creds.txt content/corpus`

`creds.txt`
These are your bot accounts' OAuth creds [CSV format, one set per line]
consumer-token,consumer-token-secret,access-token,access-token-secret

`streamcreds.py`
Store your creds for scraping Twitter here!

`TweetGenerator.py`
This scrapes Twitter, generating a corpus and tweets
An example to capture all tweets (saving them to all-tweets), and generating tweets (saving tweets to tweets-1)
`$ python3 TweetGenerator.py --corpus corpora/all-tweets --content content/tweets-1 ALL

`TwitterUser.py`
A simple class to store OAuth cres in a user-based object

`streaming.py`
This is a modified copy of the streaming class from the Tweepy API.
