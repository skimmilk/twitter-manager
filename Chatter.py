#!/usr/bin/env python3
import argparse
from queue import Queue
from threading import Thread
import tweepy
from TwitterUser import *

# if (likes/retweets) over these thresholds, the associated action will be performed
# ie; if (likes/retweets) > 3, the content will be liked, shared, and the poster will be followed
LIKE_THRESHOLD 		= 3
SHARE_THRESHOLD 	= 5
FOLLOW_THRESHOLD 	= 10

# Worker class for concurrency
class TweetWorker(Thread):
	def __init__(self, queue):
		Thread.__init__(self)
		self.queue = queue

	def run(self):
		while True:
			user, tweets = self.queue.get()

			do_things(user, tweets)

			self.queue.task_done()


# read tweets into a list from formatted .csv
def import_users(filename):

	users = []

	with open(filename) as f:
		for line in f:
			line = line.strip('\n')
			line = line.split(',')

			current_user = TwitterUser(line[0], line[1], line[2], line[3])

			users.append(current_user)

	return users


def import_tweets(filename):
	tweets = []

	with open(filename, 'r') as f:
		for line in f:
			line = line.strip('\n')

			tweets.append(line)

	return tweets


def maybe_like_tweet(api, status):
	if ( (status.favorite_count / status.retweet_count) > LIKE_THRESHOLD ):
		api.create_favorite(status.id)


def maybe_retweet_tweet(api, status):
	if ( (status.favorite_count / status.retweet_count) > SHARE_THRESHOLD ):
		api.retweet(status.id)


def maybe_follow_user(api, status):
	if ( (status.favorite_count / status.retweet_count) > FOLLOW_THRESHOLD ):
		api.create_friendship(status.user.id)


# Auth user, interact with timeline, post new tweets
def do_things(user, tweets):
	# auth user
	auth = tweepy.OAuthHandler(user.consumer_token, user.consumer_token_secret)
	auth.set_access_token(user.access_token, user.access_token_secret)
	api = tweepy.API(auth)
	authed_user = api.me()

	# get previous 20 tweets on timeline
	timeline_tweets = api.home_timeline(count=100)

	# for each status, check if should like & share tweet, or follow the user
	for status in timeline_tweets:
		if (status.retweet_count > 0): # don't deal with divide by 0

			try:
				maybe_like_tweet(api, status)
			except tweepy.error.TweepError:
				print("Cannot like status: Status already liked")

			try:
				maybe_retweet_tweet(api, status)
			except tweepy.error.TweepError:
				print("Cannot retweet status: Duplicate tweet")

			try:
				maybe_follow_user(api, status)
			except tweepy.error.TweepError:
				print("Cannot follow user: User already being followed")

	# post a tweet
	for ii in range(0, 10):
		tweet = tweets.pop()

		if (tweet):
			try:
				api.update_status(tweet)
				print('Tweeting: ' + tweet)
			except tweepy.error.TweepError:
				print("Cannot update status: Duplicate tweet")



def main():

	parser = argparse.ArgumentParser(description='Automated liking/following/retweeting and tweet of Twitter accounts')
	parser.add_argument('creds_file', help='a file that contains Twitter account OAuth credentials', metavar='creds')
	parser.add_argument('tweet_file', help='a file that contains tweets that you wish to use', metavar='content')
	parser.add_argument('threadcount', nargs='?', help='number of threads to run with (default: 2)', metavar='threads', default=2)
	args = parser.parse_args()

	num_threads = int(args.threadcount)

	creds_filename = args.creds_file
	users = import_users(creds_filename)

	content_filename = args.tweet_file;
	tweets = import_tweets(content_filename)

	num_tweets_each = ( len(tweets) // len(users) )

	if ( (len(tweets) % len(users)) != 0 ):
		num_tweets_each = num_tweets_each - 1

	# Concurrency
	queue = Queue()
	for ii in range(num_threads):
		worker = TweetWorker(queue)

		worker.daemon = True # Let main thread exit if it wants to
		worker.start()

	for index, user in enumerate(users):
		tweets_for_user = (tweets[ (index * num_tweets_each):( (index * num_tweets_each) + num_tweets_each ) ])
		queue.put( (user, tweets_for_user) )


	queue.join()


if __name__ == '__main__':
	main()
