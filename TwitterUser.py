#!/usr/bin/env python3

# This class is used to define an object for Twitter OAuth token storage - plain and simple!

class TwitterUser(object):

	def __init__(self, consumer_token, consumer_token_secret, access_token, access_token_secret):
		self.consumer_token = consumer_token
		self.consumer_token_secret = consumer_token_secret
		self.access_token = access_token
		self.access_token_secret = access_token_secret

	def __repr__(self):
		return ('Consumer token: ' + self.consumer_token +\
				'\nConsumer token secret: ' + self.consumer_token_secret +\
				'\nAccess token: ' + self.access_token +\
				'\nAccess token secret: ' + self.access_token_secret + '\n')
