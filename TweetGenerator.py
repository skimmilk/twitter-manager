#!/usr/bin/env python3

import argparse
import markovify
import os
from pathlib import Path
import sys
import random
import re
from streamcreds import *
from streaming import StreamListener
from tqdm import tqdm
import tweepy


def main():
	parser = argparse.ArgumentParser(description='Scrape twitter for a corpus and generate tweets!')
	# only supports twitter right now
	# parser.add_argument('platform', help='the platform you want to scrape', metavar='platform', choices=['twitter', 'facebook', 'linkedin'])
	parser.add_argument('--size', help='the size (in MB) that you want your corpus to be (default: 5MB)', dest='filesize')
	parser.add_argument('--corpus', help='the filename where you want to save your corpus (default: corpora/twitter-corpus)', dest='corpus_filename')
	parser.add_argument('--content', help='the filename where you want to save your content (default: content/twitter-content)', dest='content_filename')
	parser.add_argument('--tweetcount', help='the number of tweets to generate (default: 100)', dest='num_to_gen')
	parser.add_argument('keywords', nargs='*', help='keywords to filter on. Supply \'ALL\' to scrape all tweets')
	parser.add_argument('--hashtags', help='strip hashtags from the corpus', action='store_true', default=False, dest='strip_hashtags')
	args = parser.parse_args()

	# Check that corpus directory exists. If not, make one!
	corpus_dir = Path('corpora')
	if not corpus_dir.exists():
		corpus_dir.mkdir()

	# Check that content directory exists. If not, make one!
	content_dir = Path('content')
	if not content_dir.exists():
		content_dir.mkdir()


	# Check corpus argument
	if not (args.corpus_filename):
		# corpus_filename = (args.platform + '-corpus')
		corpus_filename = ('corpora/twitter-corpus')
	else:
		corpus_filename = args.corpus_filename

	# Check if corpus file exist
	corpus_exists = os.path.isfile(corpus_filename)

	# If a corpus doesn't exist, and no keywords are provided, PROBLEM!
	if not (args.keywords) and not (corpus_exists):
		print('No keywords provided')
		parser.print_help()
		sys.exit(1)

	# Check content argument
	if not (args.content_filename):
		content_filename = ('content/twitter-content')
	else:
		content_filename = args.content_filename

	# Check size argument
	if not (args.filesize):
		filesize = (5 * 1024 * 1024) # default filesize is 5MB
	else:
		filesize = (float(args.filesize) * 1024 * 1024)

	# Check tweetcount argument
	if not (args.num_to_gen):
		num_to_gen = 100
	else:
		num_to_gen = int(args.num_to_gen)

	# Join keyword args into a single string
	keywords = ' '.join(args.keywords)

	# ONLY SUPPORTS TWITTER AT THE MOMENT
	# if (args.platform == 'twitter'):
	# auth with twitter
	auth = tweepy.OAuthHandler(TWITTER_CONSUMER_TOKEN, TWITTER_CONSUMER_TOKEN_SECRET)
	auth.set_access_token(TWITTER_ACCESS_TOKEN, TWITTER_ACCESS_TOKEN_SECRET)
	# elif (args.platform == 'linkedin'):
	# 	# auth with LinkedIn
	# 	# auth??
	# elif (args.platform == 'facebook'):
	# 	# auth with Facebook
	# 	# auth ??
	# else:
	# 	print('0\tHow did I get here?')
	# 	# debugging?


	if corpus_exists:
		print('Using provided corpus: ' + str(corpus_filename))
	else:
		print('Creating ' + str(filesize / 1024 / 1024) + 'MB corpus at: ' + corpus_filename)
		create_corpus(auth, keywords, corpus_filename, filesize=filesize)

	process_corpus(args, corpus_filename)

	print('Feeding markov...')
	markov = build_markov(corpus_filename)

	print('Creating tweets...\n')
	create_tweets(content_filename, markov, num_to_gen)

	print('\n' + str(num_to_gen) + ' tweets saved to ' + content_filename)


def create_tweets(filename, markov, num_to_gen):
	f = open(filename, 'a+')

	with tqdm(total=num_to_gen) as pbar:

		for ii in range(0, num_to_gen):
			tweet = None

			while (tweet == None):
				tweet = markov.make_short_sentence(140)

			f.write(str(tweet))
			f.write('\n')

			pbar.update(1)

	f.close()


def build_markov(corpus):

	with open(corpus) as f:
		text = f.read()

	model = markovify.NewlineText(text, state_size=4)

	return model


def create_corpus(auth, keywords, filename, filesize):

	listener = StreamListener(filename=filename, filesize=filesize)
	myStream = tweepy.Stream(auth, listener)

	if keywords != 'ALL':
		myStream.filter(track=[keywords], languages=['en'], async=False)
	else:
		myStream.filter(locations=[-180,-90,180,90], languages=['en'], async=False)


def process_corpus(args, filename):

	print('Stripping links')
	strip_links(filename)

	print('Stripping mentions')
	strip_mentions(filename)

	if args.strip_hashtags:
		print('Stripping hashtags')
		strip_hashtags(filename)


def strip_links(filename):

	new_filename = filename + '.sav'

	with open(filename, 'r') as in_file, open(new_filename, 'w+') as out_file:
		for line in in_file:
			# strip links
			line = re.sub(r'https?:\/\/\S*[\s]*', '', line, flags=re.MULTILINE)

			out_file.write(line)

	os.remove(filename)
	os.rename(new_filename, filename)


def strip_mentions(filename):

	new_filename = filename + '.sav'

	with open(filename, 'r') as in_file, open(new_filename, 'w+') as out_file:
		for line in in_file:
			# strip mentions
			line = re.sub(r'@\S*[\s]*', '', line, flags=re.MULTILINE)

			out_file.write(line)

	os.remove(filename)
	os.rename(new_filename, filename)


def strip_hashtags(filename):

	new_filename = filename + '.sav'

	with open(filename, 'r') as in_file, open(new_filename, 'w+') as out_file:
		for line in in_file:
			# strip mentions
			line = re.sub(r'#\S*[\s]*', '', line, flags=re.MULTILINE)

			out_file.write(line)

	os.remove(filename)
	os.rename(new_filename, filename)


if __name__ == '__main__':
	main()
